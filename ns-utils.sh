#!/usr/bin/env bash
# ns-utils.sh
# Network Namespace utilities
# These utilities perform basic functions for network namespaces which are not currently found 
# elsewhere. Things like creating virtual ethernet interface pairs, or creating teamd instances can
# now be done quicker and easier.

# Basic logger
# Logs various types of output with details
# Arguments: errorMessage [exitCode|NOKILL]
# Note: NEW NOKILL SUPPORT!
log() {
    prefix=$(date +'%F %H:%M:%S');
    case "${1,,}" in
        'debug') printf "\E[35m${prefix} - Debug: \n${2}\e[0m\n" ;;
        'info') printf "\E[36m${prefix} - Info: ${2}\e[0m\n" ;;
        'warning') printf "\E[33m${prefix} - Warning: ${2}\e[0m\n" ;;
        'error')
            printf "\E[31m${prefix} - Error: ${2}\e[0m"
            if [ "${3^^}" != 'NOKILL' ]; then
                printf "\nWould you like to continue (Y/[N])?"
                read keepGoing              
                if [ "${keepGoing^^}" != 'Y' ]; then
                    log info "Exiting on user cancel"
                    exit "${3}"
                fi
            else
                printf "\n"
            fi
        ;;
        'critical')
            printf "\E[41m${prefix} - Critical Error: ${2}\e[0m\n"
            exit "${3}"
        ;;
    esac
}

# Namespace run
# Runs stuff in the network namespace
# Arguments: netNamespace command [comArgs]
ns_run() { ip netns exec "${1}" "${@:2}"; } 

# Namespace bash
# Runs stuff in the network namespace
# Arguments: netNamespace [bashArgs]
ns_bash() { 
    oldPS1="${PS1}"
    export PS1="[\u@\h:${1} \W]> "
    ip netns exec "${1}" /usr/bin/env bash "${@:2}"
    export PS1="${oldPS1}"
} 

# Namespace team
# Teams interfaces inside a network namespace
# Arguments: action, namespace, teamName, dev1, dev2, dev..., config
# Requires: log(), ns_run()
# Note: This is atomic, it should roll back on failure!
# Exit codes:
#   5 - Teamd states the device is busy
#   6 - Teamd configuration is incorrect
ns_team() {
    case "${1,,}" in
        'add')
            log info "Creating ${3} and starting teamd in namespace '${2}'..."
            teamdStartRes=$(ns_run "${2}" /usr/bin/teamd -donDN -t "${3}" -c "${@:$#}" 2>&1)
            teamdExitCode=$?
            
            # Basic output checking, we make this uppercase only so the input case has no impact
            case "${teamdStartRes^^}" in
                *'DEVICE OR RESOURCE BUSY'*)
                    log error "teamd is stating that ${3} is busy. Please try this manually." NOKILL 
                    log debug "${teamdStartRes}"
                    ns_team del "${@:2:(($#-2))}"
                    exit 5
                ;;
                *'FAILED TO PARSE CONFIG'*)
                    log error 'Invalid teamd configuration!' NOKILL
                    log debug "${teamdStartRes}"
                    ns_team del "${@:2:(($#-2))}"
                    exit 6
                ;;
                *)
                    if [[ teamdExitCode -eq 0 ]]; then
                        for iface in "${@:4:(($#-4))}"; do
                            log info "Moving and disabling IPv6 on ${iface} ..."
                            ip link set "${iface}" netns "${2}"
                            ns_run "${2}" ip link set "${iface}" master "${3}"
                            ns_run "${2}" sysctl -w "net.ipv6.conf.${iface}.disable_ipv6"=1 > /dev/null 2>&1
                        done

                        # Bring up team2 and disable ra
                        log info "Starting interface ${3} and disabling IPv6 autoconfig ..."
                        ns_run "${2}" sysctl -w "net.ipv6.conf.${3}.accept_ra"=0 "net.ipv6.conf.${3}.autoconf"=0 > /dev/null 2>&1
                        # We have to turn the interface completely off here, then turn it on to
                        # apply them. This appears to be a requirement of iproute2?
                        ns_run "${2}" ip link set "${3}" down
                        ns_run "${2}" ip link set "${3}" addrgenmode none
                        ns_run "${2}" ip link set "${3}" up
                    else   
                        log error 'An unknown error occurred! OH FUCK!' NOKILL
                        log debug "${teamdStartRes}"
                        ns_team del "${@:2:(($#-2))}"
                        exit 99
                    fi
                ;;
            esac
        ;;
        'del'*)
            log info "Stopping teamd and deleting ${3} in namespace '${2}' ..."
            teamdKillRes=$(ns_run "${2}" /usr/bin/teamd -k -t "${3}" 2>&1)
            ns_run "${2}" ip link delete "${3}" > /dev/null 2>&1

            for iface in "${@:4:(($#-3))}"; do
                log info "Moving ${iface} ..."
                ns_run "${2}" ip link set "${iface}" netns 1
            done
        ;;
        *)
            log critical "We ain't gots none of those fancy help docs yet." 2
        ;;
    esac
}

# Namespace veth
# Creates a veth interface pair with the second inside the network namespace
# Arguments: action, [namespace], vethName1, [vethName2]
# Requires: log(), ns_run()
# Note: This is atomic, it should roll back on failure!
# Exit codes:
#   2  - No help docs
#   96 - Unknown interface removal a
#   97 - Unknown interface removal failure
#   98 - Unknown interface creation failure 
#   99 - Unknown interface network namespace move failure
ns_veth() {
    case "${1,,}" in
        'add')
            log info "Creating interfaces ${3} and ${4} ..."
            vethAddRes=$(ip link add "${3}" type veth peer name "${4}" 2>&1)

            # Make sure it worked (WIP folks -- as errors pop up, it can be updated)
            case $? in
                0)
                    log info "Moving interface ${4} to namespace '${2}' ..." 
                    vethMovRes=$(ip link set "${4}" netns "${2}")
                    case $? in
                        0)
                            log info "Interfaces ${3} and ${4} created successfully."
                        ;;
                        *)
                            log error 'Failed to move interface!' NOKILL
                            log debug "${vethMovRes}"
                            ns_veth del "${2}" "${3}" "${4}"
                            exit 99
                        ;;
                    esac
                ;;
                *)
                    log debug "${vethAddRes}"
                    log critical 'Failed to create interfaces. Something exploded!' 98
                ;;
            esac

            log info "Starting interfaces ..."
            vethUpRes=$(ip link set up "${3}" && ns_run ${2} ip link set up "${4}")

            if [[ $? != 0 ]]; then
                log error 'Failed to start interface!' NOKILL
                log debug "${vethUpRes}"
                ns_veth del "${2}" "${3}" "${4}"
                exit 96 
            else
                log info 'Interfaces started successfully!'
            fi
        ;;
        'del')
            log info "Removing veth interfaces..."
            ipDelRes=$(ip link delete "${3}" 2>&1)

            case $? in
                0)
                    log info 'Interfaces removed successfully.'
                ;;
                *)
                    log debug "${ipDelRes}"
                    log critical 'An unknown error occurred!' 97
                ;;
            esac
        ;;
        *)
            log critical "We ain't gots none of those fancy help docs yet." 2
        ;;
    esac
}